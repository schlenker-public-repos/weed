#!/bin/bash
#
# The purpose of this script is to restore a WordPress database
# running in a Docker container from a database dump file in .gz format.
# This script assumes that it is located in the web root.
# This script also assumes that the database dump file is called
# dump.sql.gz, and the database dump file is located at C:\dbtemp. If you
# are running this script on a Mac, you'll have to adjust the file paths
# in this script accordingly.
#
# IMPORTANT NOTE: It is assumed that all occurences of the web root path in the database dump file
# have already been replaced with the web root path in the Docker container, which is
# /var/www/html. This script does not make the replacements.
#
# To run this script, navigate to the web root in a terminal window and execute the
# following command:
#
# sh restore-db-gz.sh 

# Errors are fatal
set -e

echo "# "
echo "# Commencing database restoration..."
echo "# "

echo "# Copying db dump file to web root in Docker container..."
echo "# "
docker cp /mnt/c/dbtemp/dump.sql.gz [Put database container name here]/:dump.sql.gz

echo "# Logging into Docker database container via SSH..."
echo "# "
echo "# Importing data from db dump file into database in Docker container..."
echo "# "
docker exec [Put database container name here] /bin/bash -c "zcat dump.sql.gz | mysql -u root -p[Put MySQL root password here] [Put database name here]; rm dump.sql.gz"

echo "# Database restoration complete!"
echo "# "
