#!/bin/bash
#
# The purpose of this script is to restore a WordPress database
# running in a Docker container from a database dump file in .zip format.
# This script assumes that it is located in the web root.
# This script also assumes that the database dump file is called
# dump.sql.zip and it contains a file called dump.sql, and the database dump 
# file is located at C:\dbtemp. If you are running this script on a Mac, 
# you'll have to adjust the file paths in this script accordingly.
#
# IMPORTANT NOTE: It is assumed that all occurences of the web root path in the database dump file
# have not been replaced with the web root path in the Docker container, which is
# /var/www/html. This script will make the necessary replacements. 
#
# To run this script, navigate to the web root in a terminal window and execute the
# following command:
#
# sh restore-db-zip.sh

# Errors are fatal
set -e

echo "# "
echo "# Commencing database restoration..."
echo "# "
echo "# Copying db dump file from Windows file system to Linux file system and unzipping it into a file called dump.sql..."
echo "# "
cp /mnt/c/dbtemp/dump.sql.zip .
unzip dump.sql.zip

# Uncomment the following lines and edit them as necessary if you need to replace the web root 
# file path in the dump file
#echo "# Replacing web root file path with Docker container web root file path in db dump file..."
#echo "# "
#sed -i 's+httpdocs+var\\\\/www\\\\/html+g' dump.sql

echo "# Copying db dump file to web root in Docker container..."
echo "# "
docker cp dump.sql [Put database container name here]/:dump.sql
rm dump.sql
rm dump.sql.zip

echo "# Logging into Docker database container via SSH..."
echo "# "
echo "# Importing data from db dump file into database in Docker container..."
echo "# "
docker exec [Put database container name here] /bin/bash -c "mysql -u root -p[Put MySQL root password here] [Put database name here] < dump.sql; rm dump.sql"

echo "# Database restoration complete!"
echo "# "
