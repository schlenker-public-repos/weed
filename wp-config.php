<?php

define('WP_AUTO_UPDATE_CORE', false); // Set this to true if you want WordPress updates to be installed automatically (not recommended)

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', '[Put database name here]' );

/** MySQL database username */
define( 'DB_USER', '[Put database user name here]' );

/** MySQL database password */
define( 'DB_PASSWORD', '[Put database password here]' );

/** MySQL hostname */
define( 'DB_HOST', 'db:3306' ); // Do not change this setting

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '98e088897883c6c10d8ccca102683b75db8524a0d67203f81574b01695b566c0');
define('SECURE_AUTH_KEY', '4a9e50e76ec8591f8ec5f49bb0dfe04a1239c54db76b4533d55367543c7862ba');
define('LOGGED_IN_KEY', '99eb10e48f7085e05fd3dfa5514f606d1042ac877da8f68fe31d9356950d5ac1');
define('NONCE_KEY', 'e399f744728e6fcb142602b9801fbde119ad271dcbcb71b7c296a4d6e6021d4a');
define('AUTH_SALT', 'c334bdc15a4849f2882d9f9b451f36d5061b3b19095ae2649011068820db1b6e');
define('SECURE_AUTH_SALT', '204c36ea3792e062562e141957bb793ddeef5988a6529d52d24612149cea151a');
define('LOGGED_IN_SALT', 'e8d2d2cd34d90d05ded624d77e5a2e5f2b20e1e72a04846c0c8e0f4dbf299aae');
define('NONCE_SALT', '9896eec3bddc3838ff695c45729e57ba3137ac19380b1f0edd92cdd8f3bf107f');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

define('FS_METHOD', 'direct');
define('FORCE_SSL_ADMIN', true);

/**
 * The WP_SITEURL and WP_HOME options are configured to access from any hostname or IP address.
 * If you want to access only from an specific domain, you can modify them. For example:
 *  define('WP_HOME','https://example.com');
 *  define('WP_SITEURL','https://example.com');
 *
*/
define('WP_SITEURL','https://' . $_SERVER['HTTP_HOST'] . '/');
define('WP_HOME','https://' . $_SERVER['HTTP_HOST'] . '/');

// Since we're behind a proxy server and using HTTPS, we need to alert Wordpress of that fact.
// see also http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
    $_SERVER['HTTPS'] = 'on';
}

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/**
 * If you want to convert your single site WordPress installation
 * into a multisite installation, uncomment the following lines when
 * directed to add the lines to wp-config.php in these instructions:
 *
 * https://wordpress.org/support/article/create-a-network/
 *
 * Once the lines have been uncommented, replace [Put site domain name here] 
 * with your website's domain name.
 */

// define( 'WP_ALLOW_MULTISITE', true );
// define('MULTISITE', true);
// define('SUBDOMAIN_INSTALL', false);
// define('DOMAIN_CURRENT_SITE', '[Put site domain name here]');
// define('PATH_CURRENT_SITE', '/');
// define('SITE_ID_CURRENT_SITE', 1);
// define('BLOG_ID_CURRENT_SITE', 1);

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

//  Disable pingback.ping xmlrpc method to prevent Wordpress from participating in DDoS attacks
//  More info at: https://docs.bitnami.com/general/apps/wordpress/troubleshooting/xmlrpc-and-pingback/

if ( !defined( 'WP_CLI' ) ) {
    // remove x-pingback HTTP header
    add_filter('wp_headers', function($headers) {
        unset($headers['X-Pingback']);
        return $headers;
    });
    // disable pingbacks
    add_filter( 'xmlrpc_methods', function( $methods ) {
            unset( $methods['pingback.ping'] );
            return $methods;
    });
    add_filter( 'auto_update_translation', '__return_false' );
}

if ( defined( 'WP_CLI' ) ) {
    $_SERVER['HTTP_HOST'] = 'localhost';
}
