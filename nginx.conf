server {
    listen 80;
 
    root /var/www/html;
    index index.php;
 
    access_log /var/log/nginx/access.log;
    error_log /var/log/nginx/error.log;
    proxy_connect_timeout 600; 
    proxy_send_timeout 600; 
    proxy_read_timeout 600; 
    send_timeout 600;

	# 
	#  Allow larger file uploads
	# 
	client_max_body_size 64M;
 
    location / {
        try_files $uri $uri/ /index.php?$args;
    }
 
    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass wordpress:9000;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
        fastcgi_buffers 16 16k;
        fastcgi_buffer_size 32k;
    }
    
    # Prevents 404 errors when serving up static content from child sites
    # Uncomment these lines when running WordPress multisite

    #location ~ ^.+\.(jpg|jpeg|gif|png|webp|ico|css|zip|tgz|gz|rar|bz2|doc|xls|exe|pdf|ppt|txt|tar|wav|bmp|rtf|js|JPG|JPEG|GIF|PNG|ICO|CSS|ZIP|TGZ|GZ|RAR|BZ2|DOC|XLS|EXE|PDF|PPT|TXT|TAR|WAV|BMP|RTF|JS) {
    #    if ( !-e $request_filename ) {
    #        rewrite /wp-admin$ $scheme://$host$uri/ permanent;
    #        rewrite ^(/[^/]+)?(/wp-.*) $2 last;
    #        rewrite ^(/[^/]+)?(/.*\.php) $2 last;
    #    }
    #}

    # Rewrite multisite '.../wp-.*' and '.../*.php'.
    # Prevents infinite redirect loop when accessing anything in child site wp-admin folder
    # Uncomment these lines when running WordPress multisite

    #if (!-e $request_filename) {
    #    rewrite /wp-admin$ $scheme://$host$uri/ permanent;
    #    rewrite ^/[_0-9a-zA-Z-]+(/wp-.*) $1 last;
    #    rewrite ^/[_0-9a-zA-Z-]+(/.*\.php)$ $1 last;
    #}
}
