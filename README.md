# WordPress Environment for Easy Development (WEED)

[[_TOC_]]

## Introduction

This repository provides instructions on how to set up a local WordPress website development environment powered by Docker containers. WEED uses Docker Compose to fire up six Docker containers:

1. **Database Container** - Runs the WordPress website's MySQL database using the [MySQL](https://hub.docker.com/_/mysql) image.
2. **WordPress Container** - Runs the WordPress application using the [WordPress](https://hub.docker.com/_/wordpress) image.
3. **Nginx Container** - Runs an Nginx server as a reverse proxy to provide TLS termination. Uses the [Nginx](https://hub.docker.com/_/nginx) image.
4. **WPCli** - Enables the execution of WordPress command line commands. Uses the wordpress:cli variant of the [WordPress](https://hub.docker.com/_/wordpress) image.
5. **phpMyAdmin Container** - Runs the phpMyAdmin web application, which is used to administer the MySQL database. Uses the [phpMyAdmin](https://hub.docker.com/r/phpmyadmin/phpmyadmin/) image.
6. **Https Server Container** - Runs an automated HTTPS server to enable the website to run over HTTPS. Uses the [SteveLTN/https-portal](https://github.com/SteveLTN/https-portal) image.

## WEED Setup Process

### System Requirements

The following system requirements are recommended to run WEED:

1. RAM - 16 GB minimum (32 GB recommended)
2. Hard Drive - 512 GB SSD minimum (1 TB SSD recommended)
   - WordPress websites contain a lot of files that take up a lot of hard drive space. The more hard drive space you have, the better. SSD (solid state) drives will give you better performance than standard platter hard drives.
3. Processor - Intel I5 minimum (Intel I7 or greater recommended)

### Step 1: Install Required Software

Install the software below **in the order in which it is listed**.

1. **Operating system: Windows 11 Professional or Mac**
   - If your development machine is running Windows 10 or 11, the professional version of Windows is needed for its virtualization capabilities. Windows Home won't work. For instuctions on how to upgrade Windows Home to Windows Pro, go [here](https://support.microsoft.com/en-us/windows/upgrade-windows-home-to-windows-pro-ef34d520-e73f-3198-c525-d1a218cc2818).
     - **NOTE 1:** If you upgrade from Windows Home to Windows Professional, make sure you upgrade Windows Home to the latest version and patch level before you upgrade to Windows Professional. **Important:** If you run into problems upgrading to Windows Pro, [contact Microsoft Support](https://support.microsoft.com/contactus) and have them walk you through the process.
     - **NOTE 2:** If your development machine is running Mac, be advised that these instructions are geared towards PC users, so you may have to adapt the instructions and tweak the configuration files to work for Mac.
2. **Windows Subsystem for Linux (WSL2)**
   - If your development machine is running Windows 10 or 11 Professional, you need to install [Windows Subsystem for Linux (WSL2)](https://learn.microsoft.com/en-us/windows/wsl/install), which installs a full Linux kernel on your machine. Having WSL2 installed allows your website's root directory to reside in a Linux file system on your Windows machine. Since the website root directory on the Windows machine is synchronized with the website root directory in the Docker container (which is on a Linux server), having the website root directory in the Linux file system on the host (Windows) machine will prevent a lot of folder and file permission headaches between the host machine and the Docker containers.
     - **NOTE 1:** You must upgrade Windows Home to Windows Pro **before** installing WSL2. 
     - **NOTE 2:** During the WSL2 installation process, you will be directed to install a Linux distribution. You can install whatever Linux distribution you want, but be advised that Ubuntu Linux is the distribution that was used in my setup.
     - **NOTE 3:** If WSL2, after it is installed, starts eating up all of your computer's RAM, copy the file called `.wslconfig` (located in this repository) to the root of your user profile directory (`C:\Users\[profile name]`). Doing so will limit the amount of RAM that WSL2 uses to 6 GB. I ran into this problem when I installed WSL2, and adding .wslconfig to my user profile directory solved the problem.
3. **[Docker Desktop](https://www.docker.com/products/docker-desktop)**
   - Required to run Docker containers locally.
4. **[Visual Studio Code](https://code.visualstudio.com/download) (VS Code)**
   - You can use the file editor or Integrated Development Environment (IDE) you prefer, but VS Code is strongly recommended because it integrates very well with Docker and WSL2.
   - **Required** VS Code extensions for WordPress development with WEED:
      - **Docker** by Microsoft (Install before WSL extension)
      - **Remote - WSL** by Microsoft
   - **Recommended** VS Code extensions:
      - **PHP Intelephense** by Ben Mewburn
      - **PHP Debug** by Felix Becker
      - **Prettier - Code Formatter** by Prettier
      - **Markdown Preview Enhanced** by Yiyi Wang
      - **markdownlint** by David Anson
      - **Material Icon Theme** by Philipp Kief
5. **[ConEmu terminal/console emulator](https://conemu.github.io)**
   - ConEmu is the recommended console/terminal emulator because it has several color schemes that are much easier on the eyes than those offered by terminal emulators like PuTTY, plus it allows you to have multiple terminal windows open at the same time (multiple connections to different websites at the same time).
6. **[Hosts File Editor+](https://hostsfileseditorplus.weebly.com/)**
   - Use this application to easily edit your hosts file.
7. **[Git](https://git-scm.com/downloads)**
   - Git is used for source control. It is also used to deploy changes made in the local WEED environment to the production server.

### Step 2: Perform Initial Set Up of Local WEED Environment

After you've installed and configured the software mentioned above, execute the following procedure to set up your local WEED environment:

1. Start Docker Desktop, if isn't already started.
2. Open ConEmu (referred to hereafter as the 'terminal') and start WSL by executing the `wsl` command.
3. Create a directory on the development machine that will be used as the website root directory.
    - **IMPORTANT:** If you are running WSL2 on Windows 10 or 11 Pro, the website root directory must be created in the Linux file system, not the Windows file system. If you want to access the C drive on the Windows file system from the Linux file system, navigate to `/mnt/c/`. To navigate to the home directory on the Linux file system, execute the following command in a terminal: `cd ~`. Everything in and below this directory is located in the Linux file system. Exactly where under `~` you create the website root directory is up to you. Create a directory structure that makes the most sense to you.
4. Download the contents of this repository in a .zip file to your hard drive and unzip it. Copy the files listed below into the website root directory in the Linux file system. NOTE: If you're on a Windows machine, you'll have to download the .zip file to a folder in the Windows file system first, extract the contents of the zip file to that directory, and then copy the extracted files over to the directory you created in Linux file system. To copy the files, open a terminal and cd into the directory you created in the Linux file system, and then use the cp (copy) command to copy the files from the directory on the Windows file system to the directory in the Linux file system. For example, if you downloaded the .zip file to C:\temp\myfolder and extracted it there, you could copy the files from that directory to a directory called "mysite" in the Linux file system using the following command (assuming you have cd'ed into the "mysite" directory in the terminal): `cp -a /mnt/c/temp/myfolder/. .`
    - `.mygitignore`
      - **IMPORTANT:** Rename the copy of the file to `.gitignore`
    - `.dockerignore`
    - `README.md`
    - `docker-compose.yml`
    - `nginx.conf`
    - `php-uploads.ini`
    - `restore-db-gz.sh`
    - `restore-db-zip.sh`
    - `set-perms.sh`
    - `wp-config.php`
5. If you're on a machine running Windows 10 or 11 Pro, copy the following file in this repository into the root of your user profile directory in C:\Users[your profile name]:
    - `.wslconfig`
      - This file will prevent WSL2 from eating all of your RAM.
6. Clear the contents of the file README.md by executing the following command from the command line in the website root directory:
    - `truncate -s 0 README.md`
7. Open `wp-config.php` and make the following edits:
    - Replace `[Put database name here]` with the desired name of the MySQL database.
    - Replace `[Put database user name here]` with the desired MySQL database user name.
    - Replace `[Put database password here]` with the desired MySQL database password.
    - Update value of `$table_prefix` as desired.
8. Open `docker-compose.yml` in VS Code and make the following edits:
    - Replace `[Put root user password here]` with the desired MySQL root user password.
    - Replace `[Put database name here]` with the desired name of the MySQL database. This needs to match the value set into the DB_NAME variable in wp-config.php.
    - Replace `[Put MySQL user name here]` with the desired MySQL user name. This needs to match the value set into the DB_USER variable in wp-config.php.
    - Replace `[Put MySQL password here]` with the desired MySQL database password. This needs to match the value set into the DB_PASSWORD variable in wp-config.php.
    - Replace `[Put domain here]` with the desired domain name for your website.
9. Open your hosts file in Host File Editor+ (or whatever hosts file editor you prefer) and add the following entry:
    - `127.0.0.1 yourwebsitedomainname.com`
    - Replace `yourwebsitedomainname.com` with the domain name of your website. If you don't have a domain name yet, make one up.
    - Comment out the hosts file entry using a `#` tag until you're ready to fire up WEED.
10. Open `restore-db-gz.sh` and make the following edits:
    - **NOTE**: This file can be used to restore the data in the database running in the database Docker container from a database dump file that is in .gz format.
    - Replace all occurrences of `[Put database container name here]` with the name of the MySQL database Docker container. The MySQL database container name should be `[root folder]_db_1`, where `[root folder]` is the name of the website's root directory/folder. For example, if the root folder is called `mysite`, the MySQL database container name would be `mysite_db_1`.
    - Replace `[Put MySQL root password here]` with the password of the MySQL root user.
    - Replace `[Put database name here]` with the password of the root user.
11. Open `restore-db-zip.sh` and make the following edits:
    - **NOTE**: This file can be used to restore the data in the database running in the database Docker container from a database dump file that is in .zip format.
    - Replace all occurrences of `[Put database container name here]` with the name of the MySQL database Docker container.
    - Replace `[Put MySQL root password here]` with the password of the MySQL root user.
    - Replace `[Put database name here]` with the password of the root user.
12. If you're on a Windows 10 machine, create a new folder at the root of the C drive called `dbtemp`. If you're on a Mac, create a folder called `dbtemp` in the location of your choice, and update the path to the folder in `restore-db-gz.sh` and `restore-db-zip.sh`. 
    - **NOTE**: The `dbtemp` folder will be used to store a database dump file that will be used to restore the database running in the Docker container. The database will be restored from the dump file by running the `restore-db-gz.sh` script or the `restore-db-zip.sh` script as appropriate.

### Step 3: Fire Up the Local WEED Environment for the First Time

Use the procedure below to fire up WEED for the first time:

1. Start Docker Desktop, if it isn't started already.
2. Open a terminal (ConEmu, etc.).
3. Execute the `wsl` command.
4. Change directory into your website root directory.
   - The directory should located in the Linux file system. If your development machine is a PC, anything under `~` (home directory) is in the Linux file system.
5. Execute the following command to start the website:
   - `docker-compose up -d`
      - Executing this command will start all Docker containers. The first time this command is run for a website, you'll see various pieces of software being installed. This is normal and will only occur during the initial start up.
6. Verify that all six Docker containers started successfully by executing the following command:
   - `docker ps`
      - If a container started successfully, you'll see the following text in the "Status" column next to the container:
         - `Up X seconds/minutes/hours`
      - **IMPORTANT NOTE**: The WP Cli container will start and then immediately shut down. **THIS IS NORMAL**. It isn't supposed to continue running like the other containers.
7. Open the hosts file in Host File Editor+ or the editor of your choice. Uncomment the entry you made for the website you're running in the Docker containers.
8. Open a web browser and navigate to the URL for your website that was specified in the hosts file entry. Make sure the protocol of the URL is https, not http.
   - For example, if you specified the URL `mycoolwebsite.com` in the hosts file, you'll navigate to the following URL in your browser:
      - `https://mycoolwebsite.com`
   - NOTE: If you receive a `Connection could not be established` error, wait for a few seconds, and it should go away. You will receive the error if you try to access the website before the containers are fully fired up.
9. The first time you access the website during the current browser session, you will see a security warning that says something like "Your connection is not private". The reason you're seeing the message is because the web server is using a self-signed SSL certificate. Since this is just a local development environment, you can safely disregard the warning. Click on the "Advance" button, and then click the link to proceed to the website.
10. The next thing you should see is the first screen of the Famous WordPress 5-Minute Install, which is a wizard that you complete in order to install WordPress on the server.
11. Select your preferred language and click the "Continue" button.
12. Fill out the form that is displayed and click the "Install WordPress" button.
13. Log into the website using the username and password you set into the form on the previous screen.
14. Once you've logged in, you'll be taken to the admin dashboard. It is recommended that you perform the following tasks:
    - Delete any inactive themes that you don't plan to use.
    - Update plugins as needed.
      - Install the Wordfence plugin. This plugin will prevent dirtbags from hacking your website. Don't activate the plugin until you deploy your website to production, because it won't work if your website doesn't have a domain pointing to it.
    - Update themes as needed.
    - Update WordPress if an update is available.
15. When you're done with the initial configuration and updating of the website, log off.
16. Execute the following steps to verify that you can access the phpMyAdmin web application that you will use to administer the website's database:
    - In the web browser, navigate to `http://[your domain]:8181`, where `[your domain]` is the domain name of your website. Note that the protocol is http, not https.
    - On the login screen, enter the following information:
       - **Server**: db
       - **Username**: The database user you provided in wp-config.php.
       - **Password**: The database password you provided in wp-config.php.
    - Click the `Go` button to log in.
    - Once you're logged into phpMyAdmin, expand the name of the website database in the left-hand navigation menu to see a list of tables in the database. Select a table to view its contents.
    - When you're done, click the logout button in the upper-left hand corner of the screen.
17. Execute the following steps to verify that you can SSH into the Docker containers:
    - In ConEmu, enter the command `docker exec -it [container name] /bin/bash` where `[container name]` is the name of the Docker container you're trying to SSH into. If you aren't sure what that name of the container is, execute the command `docker ps`.
    - To terminate the SSH session, type `exit`.
18. In ConEmu, shut down the Docker containers by executing the command `docker-compose down`.

### Step 4: Create Git Repository

Use the procedure below to convert the website's root directory into a Git repository:

1. In ConEmu, navigate to the website's root directory and execute the command `git init`. This will convert the root directory to a Git repository.
2. Follow [these instructions](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup) to do the initial configuration of the Git repository.
3. Perform the initial add and commit of the repository. Before you do, make sure the website's root directory contains the `.gitignore` file. If the file is still called `.mygitignore`, rename it to `.gitignore` before you do the first add and commit.
4. Push the repository to a remote Git repository on a Git repository provider like GitLab or GitHub.

### Step 5: Set Directory and File Ownership and Permissions

Use the procedure below to set the directory and file ownership and permissions needed to run the website in Docker containers:

1. In ConEmu, execute the command `sudo sh set-perms.sh` in the website root directory to run a script that sets the necessary permissions on the files and folders in the website root directory. If this script is not run, the website will throw permissions errors when you try to upload images to the website's media library, and you may get a fatal error when you log into the website's admin dashboard. Executing the script will open up the directory permissions so that these problems won't occur. **IMPORTANT NOTE #1**: Do not run this script in the production environment. It is only meant to be run in a local development environment. **IMPORTANT NOTE #2**: You must run this command while logged in as a user with sudo permissions. In order to execute the command with sudo permissions, the user account you're using must be a member of the sudoers group. If you're not a member of the sudoers group, you must log in as a user that is a member of the sudoers group, or you must log in as the root user. Once you're logged in as a user that is a member of the sudoers group or the root user, execute the command `sudo sh set-perms.sh` to run the script.

### Optional: Create a Multisite WordPress Network

After executing the procedure above, you will have what is known as a single site WordPress website. If you want to convert the single site WordPress website into a multisite WordPress network, use [these instructions](https://wordpress.org/support/article/create-a-network/). **Read the notes below before you do the conversion.**

**NOTE 1**: The code that the instructions ask you to add to wp-config.php has already been added, but it is commented out. When the instructions direct you to add the code to wp-config.php, just uncomment the code that's already there and modify it as necessary.

**NOTE 2**: The instructions ask you to add code to a file called `.htaccess`, but that file isn't needed when you are running in your local WEED environment, because WEED uses an Nginx server instead of an Apache server. Your project contains a file called `nginx.conf`, which serves the same purpose for Nginx servers that `.htaccess` serves for Apache servers. If you deploy your website to an Apache server, you will need to add an `.htaccess` file containing the code specified in the instructions.

**NOTE 3**: When the instructions ask you to add code to the `.htaccess` file, uncomment the multisite specific code at the bottom of the `nginx.conf` file instead.

**NOTE 4**: If you just want to create a sub-directory multisite network, and don't have time to mess around, you can create one quickly by using the following TLDNR version of the instructions mentioned above:

1. Start your website (`docker-compose up -d` in ConEmu).
2. Go to the admin dashboard and configure the website to use Pretty Permalinks.
   - Navigate to `Settings --> Permalinks`
   - Select the radio button next to `Day and name`.
   - Click the `Save Changes` button.
3. Disable all plugins.
4. Uncomment the line `define( 'WP_ALLOW_MULTISITE', true );` in the multisite section in wp-config.php. Leave the remaining lines in that section commented for now.
5. Refresh your browser.
6. In the left-hand menu in the admin dashboard, select `Tools --> Network Setup`.
In the screen that appears, make sure `Sub-directories` is selected. Edit the remaining fields as desired.
7. Click the `Install` button.
8. In wp-config.php, uncomment the remaining commented lines in the multisite section. Comment out the line `define( 'WP_ALLOW_MULTISITE', true );`. This will remove the no longer needed `Network Setup` option in the `Settings` menu of the network admin dashboard.
9. Save and exit wp-config.php.
10. In `nginx.conf`, uncomment all commented multisite related code. Save and exit the file.
11. Log out of the website, shut down the Docker containers, restart the Docker containers, and log back into the website.
12. Make sure the main (parent) website renders correctly.
13. Create a new child website by doing the following:
    - In the Network admin dashboard, select `Sites --> Add New`.
    - Fill out the form and click the `Add Site` button.
    - Select `My Sites --> [Name of your child site] --> Dashboard`. Make sure the dashboard of the child website displays without errors.
    - Visit the child website and make sure it renders correctly.
14. Re-enable all plugins.

## WEED Workflow

Use the following workflow to get the most out of WEED:

### Update Files and Restore Database from Production Server

Before you develop enhancements or bug fixes for your website, it is recommended that you pull the latest files from the production server and restore the local database from a dump of the production database. Use the following procedure to accomplish these tasks:

1. Perform any available plugin, theme, and Wordpress updates in the admin dashboard on the production website.
   - If you install any updates on the production server, open ConEmu and execute the command `git pull origin [primary branch name]` in the website root directory in your local WEED environment to pull down any file changes. Replace [`primary branch name]` with the name of your primary branch ('master', 'main', etc.).
2. Export a dump of the production database to a dump file and download it to your computer. The dump file should be in .zip or .gz format.
3. Rename the dump file `dump.sql.zip` or `dump.sql.gz`, depending on the format of the file.
4. Copy the dump file to the `dbtemp` directory on your computer that you created earlier.

### Fire up WEED

1. Start Docker Desktop.
2. Launch WSL (Windows users only)
   - Open ConEmu.
   - Type `wsl` and press the enter key.
3. In ConEmu, change directory into the website's root directory.
4. Start the website by executing the command `docker-compose up -d`. This will start all Docker containers.
5. If you downloaded a database dump file that was exported from the production database, execute the command `sh restore-db-zip.sh` if the dump file is in `.zip` format, or execute the command `sh restore-db-gz.sh` if the dump file is in `.gz` format. This script will copy the dump file from the `dbtemp` directory on the host file system into the database Docker container, and import it into the database running in the database Docker container.
6. Open Host File Editor+ and uncomment the hosts file entry for the website. Save the change.
7. Open a web browser, navigate to the website, and log in.

### Prepare to Develop the Website

1. Create Git feature branch, if necessary.
   - Before you make any changes to your website, it is recommended that you create a feature branch in the Git repository and make your changes on the feature branch. The master branch should only contain tested production code.
   - Whenever you save a change to a file in the website's root directory on your development machine, the change is immediately synchronized with the website root directory in the WordPress Docker container. Once the change has been synchronized, it will be visible in the web browser. The synchronization normally takes less than one second.
2. Open the website root directory in VS Code.
   - Open VS Code.
   - Click the Remote Explorer button/icon in the left-hand navigation bar. If it isn't there, you probably need to install the Remote WSL extension.
   - If there are no Linux distributions listed in the pane that appears, do the following:
      - Hover your mouse cursor over `WSL TARGETS` and click the `+` symbol that appears to add a Linux distribution.
   - Right-click the Linux distribution and select "Connect to WSL".
   - Click the "Open Folder" button/icon.
   - Navigate to and select the folder that is the root directory of your website.
   - Click the "OK" button.

### Develop the Website and Deploy Changes

Once you've finished making and testing your changes, push the feature branch to the remote repository on the production server. After you've thoroughly tested the changes and are ready to deploy them, merge the feature branch on the production server into the master branch.

### Extinguish (Shut Down) WEED

1. Log out of the website.
2. Shut down the Docker containers by executing the following command in ConEmu in the website root directory:
   - `docker-compose down`
3. In the hosts file editor, comment out the entry for your website. Save your changes and exit the editor.
4. Exit VS Code
   - Select `File --> Close Remote Connection`
   - Select `File --> Exit`
5. Shut down Docker Desktop by right-clicking the Docker icon in the system tray and selecting `Quit Docker Desktop`.
6. Add, commit, and push any uncommitted Git changes as necessary.
7. Exit WSL
   - In ConEmu, execute the `exit` command.
   - Shut down WSL by executing the command `wsl --shutdown`
   - Execute the `exit` command one more time to exit Cmder.
