#!/bin/bash
#
# The purpose of this script is to set directory and file ownership and
# permissions on the web root of a WordPress website that runs in a local
# Docker container. The script opens up the permissions so that the web server user
# (assumed to be www-data) can create or edit files and directories as needed. This 
# script is not to be run on a production website. It is intended to be run in Docker-based
# development environments only.
#

# Errors are fatal
set -e

# Set core file mode to false. This must be done to prevent git from thinking that all
# files have been changed after the permissions of all directories has been opened
# up to 777.
echo "# "
echo "# Setting core file mode..."
git config core.fileMode false
echo "# "
echo "# Done!"
echo "# "

# Change ownership of all WordPress core files in the web root 
# directory except wp-config.php
echo "# Setting ownership of WordPress core files in web root..."
sudo find wp-*.php \
   \( -name wp-config.php \
   \) -prune \
   -o -type f -exec sudo chown www-data:www-data {} \;
echo "# "
echo "# Done!"
echo "# "

# Change ownership of all files and directories in the wp-admin, 
# wp-content, and wp-includes directories, except the node_modules
# directory
echo "# Setting directory and file ownership..."
sudo find ./wp-content ./wp-admin ./wp-includes \
   \( -name node_modules \
   \) -prune \
   -o -exec sudo chown www-data:www-data {} \;
echo "# "
echo "# Done!"
echo "# "

# Change permissions of all directories in the wp-admin, 
# wp-content, and wp-includes directories, except the node_modules
# directory
echo "# Setting directory permissions..."
sudo find ./wp-content ./wp-admin ./wp-includes \
   \( -name node_modules \
   \) -prune \
   -o -type d -exec chmod 777 {} \;
echo "# "
echo "# Done!"
echo "# "

# Change permissions of all files in the wp-admin, 
# wp-content, and wp-includes directories, except the files in the
# node_modules and vendor directories
echo "# Setting file permissions..."
sudo find ./wp-content ./wp-admin ./wp-includes \
   \( -name node_modules \
   \) -prune \
   -o -type f -exec chmod 666 {} \;
echo "# "
echo "# Done!"
echo "# "
echo "# Directory and file ownership and permissions successfully set!"
echo "# "
